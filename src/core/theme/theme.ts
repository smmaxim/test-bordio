export interface styleVars {
  fontFamily: string,
  color: {
    default: string
  },
  button: {
    primary: string,
  }
}

export const styleVars = {
  fontFamily: 'Roboto, Open-Sans, Helvetica, Sans-Serif',
  color: {
    default: '#222222',
  },
  button: {
    primary: '#0094FF',
  }
}