import { createGlobalStyle } from 'styled-components'
// import { styleVars } from './'

//Todo
export const GlobalStyle = createGlobalStyle`
html, body {
  margin: 0;
  padding: 0;
}
body {
  font-family: Roboto, Open-Sans, Helvetica, Sans-Serif;
  color: #222222;
  background-color: #102250;
  &.overflow {
    overflow: hidden;
  }
}
& * {
  box-sizing: border-box;
}
& a {
  color: #0094FF;
  text-decoration: none;
  &:hover, &:focus {
    text-decoration: underline;
  }
}
`