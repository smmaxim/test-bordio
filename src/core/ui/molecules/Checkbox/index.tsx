import React from 'react'
import styled from 'styled-components'
import { Label } from './../../'
import { Icons } from './../../../icons'

export interface Checkbox extends React.HTMLAttributes<HTMLInputElement> {
  checked: boolean,
  required?: boolean
}

export const Checkbox: React.FC<Checkbox> = ({ children, checked = false, required, ...props }) => (
  <Caption>
    <CaptionLabel>
      <Switch>{checked && <Icons.check />}</Switch>
      <input checked={checked} type="checkbox" hidden required={required} {...props} />
      <Title>{children}</Title>
    </CaptionLabel>
  </Caption>
)

const Caption = styled.div`
  cursor: pointer;
  .icon-check {
    svg {
      path {
        stroke: #0094FF
      }
    }
  }
`
const CaptionLabel = styled(Label)`
  display: flex;
  align-items: center;
`
const Switch = styled.span`
  display: inline-block;
  background: #FFFFFF;
  border: 1px solid #0094FF;
  border-radius: 3px;
  width: 14px;
  height: 14px;
  margin-right: 8px;
`

const Title = styled.span``