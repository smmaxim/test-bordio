import React, { useState, cloneElement, useCallback } from 'react'
import styled from 'styled-components'
import { Backdrop } from './../../'
import { Icons } from './../../../icons'

export type valueType = { text: string, value: string | number };

export interface Dropdown {
  label: string,
  value?: valueType | null
  onChange?: (value: valueType | null) => void
}

export const Dropdown: React.FC<Dropdown> = ({
  label = '',
  value = null,
  onChange,
  children
}) => {

  const [isOpen, toggleOpen] = useState<boolean>(false)

  const handleToggleOpen = (): void => toggleOpen(state => !state);

  const handleClick = useCallback((currentValue: valueType) => {
    onChange && onChange(currentValue.value === value?.value ? null : currentValue);
    handleToggleOpen();
  }, [value, onChange])

  return (
    <DropdownCaption>

      <DropdownControl className={!!value ? 'has-active' : ''} onClick={handleToggleOpen}>
        {value ? value.text : label}
        <DropdownArrow up={isOpen}><Icons.arrowDown /></DropdownArrow>
      </DropdownControl>

      {isOpen && (
        <>
          <DropdownMenu>
            {React.Children.map(children, child => React.isValidElement(child) && cloneElement(child, {
              isActive: child.props.value === value?.value,
              onClick: () => {
                const { value = '', children: text = '' } = child.props;
                handleClick({ value, text });
              }, ...child.props
            }))}
          </DropdownMenu>

          <Backdrop onClick={handleToggleOpen} style={{ backgroundColor: 'transparent' }} />
        </>
      )}

    </DropdownCaption>
  )

}

const DropdownCaption = styled.div`
  position: relative;
`;

//Todo
const DropdownControl = styled.div`
  height: 50px;
  background-color: #F5F8FA;
  color: #A2A2A2;
  border-radius: 8px;
  font-size: 14px;
  padding: 18px;
  cursor: pointer;
  position: relative;
  z-index: 11;
  &.has-active {
    color: #222222;
  }
`;
const DropdownMenu = styled.div`
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  margin-top: 6px;
  z-index: 11;
  background-color: #FFFFFF;
  box-shadow: 0px 3px 8px #00000026;
  border-radius: 8px;
  overflow: hidden;
  max-height: 200px;
  overflow: auto;

  &::-webkit-scrollbar {
    width: 8px;
  }
  &::-webkit-scrollbar-track {
    box-shadow: none;
    background-color: #F5F8FA;
  }   
  &::-webkit-scrollbar-thumb {
    background-color: darkgrey;
  }
`;

export interface DropdownItem {
  value: string | number,
  isActive?: boolean
}

export const DropdownItem = styled.div<DropdownItem>`
  display: block;
  font-size: 14px;
  padding: 10px 20px;
  cursor: pointer;
  &:hover, &:focus {
    color: #737373;
  }
  ${({ isActive }) => isActive && 'background-color: #F5F8FA;'}
`

interface DropdownArrow {
  up: boolean
}
const DropdownArrow = styled.span<DropdownArrow>`
  position: absolute;
  width: 11px;
  height: 11px;
  top: 50%;
  margin-top: -5px;
  right: 20px;
  ${({ up }) => up && `
    transform: rotate(180deg);
  `}
`