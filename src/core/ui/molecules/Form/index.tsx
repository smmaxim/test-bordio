import React from 'react'
import styled from 'styled-components'
import { Error } from './../../'

export const Form = styled.form``;

export interface FormGroup {
  error?: string
}

export const FormGroup: React.FC<FormGroup> = ({
  error,
  children
}) => (
  <Caption>
    {children}
    {!!error && <StyledError>{error}</StyledError>}
  </Caption>
)

export const Caption = styled.div`
  & + & {
    margin-top: 25px;
  }
`;

export const StyledError = styled(Error)`
  margin-left: 18px;
  margin-top: 2px;
`;
