import React from 'react'
import styled from 'styled-components'
import { Input } from './../../'

export interface InputGroup extends React.HTMLAttributes<HTMLInputElement> {
  iconLeft?: React.ReactNode,
  type?: string
  required?: boolean
  value?: string
  name?: string
}

export const InputGroup: React.FC<InputGroup> = ({ iconLeft, type = 'text', value, ...props }) => (
  <Caption className={iconLeft ? 'has-icon' : ''}>
    {iconLeft && <StyledIcon>{iconLeft}</StyledIcon>}
    <Input value={value} type={type} {...props} />
  </Caption>
)

const Caption = styled.div`
  position: relative;
  &.has-icon input {
    padding-left: 52px;
  }
`

const StyledIcon = styled.div`
  width: 20px;
  position: absolute;
  top: 50%;
  margin-top: -10px;
  margin-left: 18px;
  display: flex;
  align-items: center;
`