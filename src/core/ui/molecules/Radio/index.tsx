import React from 'react'
import styled from 'styled-components'
import { Label } from './../../'

type valueType = string | number;

export interface Radio extends React.HTMLAttributes<HTMLInputElement> {
  name: string
  value: valueType
  currentValue: valueType
  required?: boolean
}


export const Radio: React.FC<Radio> = ({ children, name, value, required, currentValue, ...props }) => {

  const isChecked = value === currentValue;

  return (
    <Caption>
      <CaptionLabel>
        <input checked={isChecked} value={value} type="radio" name={name} hidden {...props} />
        <Switch checked={isChecked}></Switch>
        <Title>{children}</Title>
      </CaptionLabel>
    </Caption>
  )

}
const Caption = styled.span`
  display: inline-block;
  cursor: pointer;
  .icon-check {
    svg {
      path {
        stroke: #0094FF
      }
    }
  }
  & + & {
    margin-left: 25px;
  }
`

const CaptionLabel = styled(Label)`
  display: flex;
  justify-content: center;
  align-items: center;
`

interface Switch {
  checked: boolean
}

const Switch = styled.span<Switch>`
  background: #FFFFFF;
  border: 1px solid #0094FF;
  border-radius: 50%;
  width: 14px;
  height: 14px;
  margin-right: 8px;
  display: block;
  position: relative;
  ${({ checked }) => checked && `
    &:after {
      content: '';
      width: 8px;
      height: 8px;
      border-radius: 50%;
      display: block;
      background-color: #0094FF;
      position: absolute;
      top: 50%;
      left: 50%;
      margin-top: -4px;
      margin-left: -4px;
    }
  `}
`

const Title = styled.span``