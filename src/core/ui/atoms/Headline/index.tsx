import React, { createElement } from 'react'

export type VariantType = 'h1' | 'h2' | 'h3' | 'div'
export interface Headline extends React.HTMLAttributes<HTMLElement> {
  variant?: VariantType
  component?: VariantType
}
export const Headline: React.FC<Headline> = ({ variant = 'h1', component = 'h1', children, ...props }) => {
  return createElement(component, props, children)
}
