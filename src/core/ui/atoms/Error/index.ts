import styled from 'styled-components'

export const Error = styled.div`
    font-size: 10px;
    color: #E82828;
`;