import styled from 'styled-components'

export const Input = styled.input`
    display: block;
    width: 100%;
    padding: 16px 18px;
    font-size: 14px;
    line-height: 1.5;
    color: #222222;
    background-color: #F5F8FA;
    border: 1px solid #F5F8FA;
    border-radius: 8px;
    height: 50px;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    &::placeholder {
        color: #A2A2A2;
    }
`;