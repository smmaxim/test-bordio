import styled from 'styled-components'

type variant = 'default' | 'primary'

export interface Button {
  variant?: variant,
  fullWidth?: boolean,
  disabled?: boolean
}

export const Button = styled.button<Button>`
  border-radius: 30px;
  background-color: transparent;
  font-size: 18px;
  color: #000;
  padding: 20px 40px;
  cursor: pointer;
  display: inline-block;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  user-select: none;
  border: 1px solid transparent;
  transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
  min-height: 62px;

  ${({ variant }) => variant === 'primary' && `
    background-color: #0094FF;
    color: #fff;
  ` }
  
  ${({ fullWidth }) => fullWidth && `
    display: block;
    width: 100%;
  ` }

  ${({ disabled }) => disabled && `
      background-color: #A2A2A2;
      pointer-events: none;
      cursor: no-drop;
  ` }

  &:focus {
    outline: none;
    box-shadow: none;
  }

`;