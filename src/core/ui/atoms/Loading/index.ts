import styled, { keyframes } from 'styled-components'

const rotate = keyframes`
  to { transform: rotate(360deg); }
`;

export interface Loading {
  size?: number
}

export const Loading = styled.div<Loading>`
  width: ${({ size = 40 }) => size + 'px'};
  height: ${({ size = 40 }) => size + 'px'};
  border-radius: 50%;
  background: transparent;
  border-top: 4px solid #fff;
  border-right: 4px solid #fff;
  border-bottom: 4px solid #777;
  border-left: 4px solid #777;
  animation: ${rotate} 1.2s infinite linear;
  display: inline-block;
`;