import React from "react";
import styled from 'styled-components'

export const Icons = {
    arrowDown: () => (
        <Caption className="icon icon-arrowDown">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={11}
                height={11}
                viewBox="0 0 11 11"
            >
                <path
                    d="M3.056 3.699a.453.453 0 01.313.127l2.62 2.513 2.625-2.421a.447.447 0 01.36-.122.476.476 0 01.329.206.619.619 0 01.111.412.587.587 0 01-.18.378L6.297 7.499a.442.442 0 01-.621 0l-2.934-2.8a.589.589 0 01-.175-.38.617.617 0 01.111-.41.468.468 0 01.376-.207z"
                    fill="#222"
                />
            </svg>
        </Caption>
    ),
    lock: () => (
        <Caption className="icon icon-lock">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={16}
                height={19.89}
                viewBox="0 0 16 19.89"

            >
                <defs>
                    <style>{".prefix__a{fill:#babcbe}"}</style>
                </defs>
                <path
                    className="prefix__a"
                    d="M13 5.89h-1V4a4 4 0 00-8 0v1.89H3a3 3 0 00-3 3v8a3 3 0 003 3h10a3 3 0 003-3v-8a3 3 0 00-3-3zM6 4a2 2 0 114 0v1.89H6zm8 12.89a1 1 0 01-1 1H3a1 1 0 01-1-1v-8a1 1 0 011-1h10a1 1 0 011 1z"
                />
                <path
                    className="prefix__a"
                    d="M8 9.89a3 3 0 103 3 3 3 0 00-3-3zm0 4a1 1 0 111-1 1 1 0 01-1 1z"
                />
            </svg>
        </Caption>
    ),
    mail: () => (
        <Caption className="icon icon-mail">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={20}
                height={16}
                viewBox="0 0 20 16"
            >
                <path
                    d="M17 0H3a3 3 0 00-3 3v10a3 3 0 003 3h14a3 3 0 003-3V3a3 3 0 00-3-3zm-.67 2L10 6.75 3.67 2zM17 14H3a1 1 0 01-1-1V3.25L9.4 8.8a1 1 0 001.2 0L18 3.25V13a1 1 0 01-1 1z"
                    fill="#babcbe"
                />
            </svg>
        </Caption>
    ),
    check: () => (
        <Caption className="icon icon-check">
            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path
                    clipRule="evenodd"
                    fill="none"
                    stroke="#000"
                    strokeMiterlimit={10}
                    strokeWidth={2}
                    d="M20 6L9 17l-5-5"
                />
            </svg>
        </Caption>
    )
};

const Caption = styled.span`
    svg {
        width: 100%;
        height: 100%;
    }
`;