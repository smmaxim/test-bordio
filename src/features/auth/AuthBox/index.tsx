import React from 'react'
import styled from 'styled-components'

export interface AuthBox {
  title: string
}

export const AuthBox: React.FC<AuthBox> = ({
  title = '',
  children
}) => (
  <Caption>
    <Title>{title}</Title>
    {children}
  </Caption>
)

export const Caption = styled.div`
  padding: 30px 30px 50px 30px;
  border-radius: 8px;
  background-color: #fff;
  max-width: 400px;
  width: 100%;
  min-width: 250px;
`

export const Title = styled.h4`
  display: block;
  text-align: center;
  font-size: 28px;
  font-weight: bold;
  margin: 0 0 35px 0;
`