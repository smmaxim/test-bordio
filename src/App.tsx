import React from "react";
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, styleVars } from './core/theme'
import { AuthRegistrationPage } from './pages'
import "@fontsource/roboto"

function App() {
  return (
    <ThemeProvider theme={styleVars}>
      <GlobalStyle />
      <AuthRegistrationPage />
    </ThemeProvider>
  );

}

export default App;
