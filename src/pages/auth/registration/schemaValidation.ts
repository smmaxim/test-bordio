import * as yup from 'yup';

export const schema = yup.object().shape({
  name: yup.string().matches(/^[A-Za-z]*$/, 'Please enter a valid name').required('Name is required'),
  email: yup.string().email('Please enter a valid email address').required('Email is required'),
  password: yup.string().min(6, 'Password must contain at least 6 symbols').required('Please enter a valid password'),
  country: yup.string().required('You must select your country'),
  gender: yup.string().required('You must select the gender'),
  accept: yup.bool().oneOf([true], 'You must accept the policies').required('You must accept the policies'),
});