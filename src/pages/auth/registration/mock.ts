export const countries = [
  {
    "value": "6016fa7ba7c1ecf2a822258d",
    "text": "Martinique"
  },
  {
    "value": "6016fa7b607fb1bc3b9a688c",
    "text": "Azerbaijan"
  },
  {
    "value": "6016fa7b007508cb0eeeb49b",
    "text": "French Southern Territories"
  },
  {
    "value": "6016fa7bd9e3f4b11821d6f6",
    "text": "Hungary"
  },
  {
    "value": "6016fa7ba11282ccaaf3ba9a",
    "text": "Turks and Caicos Islands"
  },
  {
    "value": "6016fa7bc431152ac6258538",
    "text": "Lithuania"
  },
  {
    "value": "6016fa7baf97ff19378ca566",
    "text": "Madagascar"
  },
  {
    "value": "6016fa7be166a6b7aed4a552",
    "text": "Turkey"
  },
  {
    "value": "6016fa7b89e8401286e32ca6",
    "text": "Aruba"
  },
  {
    "value": "6016fa7b089c8fd00faaaef3",
    "text": "Liechtenstein"
  },
  {
    "value": "6016fa7b0b0d790f7a2dc522",
    "text": "Cuba"
  },
  {
    "value": "6016fa7bfd068c5f4ca58b42",
    "text": "Iran"
  },
  {
    "value": "6016fa7b5df9ef453e137a11",
    "text": "Tunisia"
  },
  {
    "value": "6016fa7ba9b79a3e939d07a6",
    "text": "Colombia"
  },
  {
    "value": "6016fa7b49862fe5b100c8cd",
    "text": "Burkina Faso"
  },
  {
    "value": "6016fa7bed3d3ae22b796d9a",
    "text": "Equatorial Guinea"
  },
  {
    "value": "6016fa7bcf3901eb96eab434",
    "text": "Kiribati"
  },
  {
    "value": "6016fa7b57efd83b11608966",
    "text": "Algeria"
  },
  {
    "value": "6016fa7bedd58cac8fc8a5f6",
    "text": "Benin"
  },
  {
    "value": "6016fa7b49574ccf8f11404e",
    "text": "Solomon Islands"
  },
  {
    "value": "6016fa7b7d13cd9c0f89b622",
    "text": "Brunei Darussalam"
  },
  {
    "value": "6016fa7baf11b6914dd46720",
    "text": "Anguilla"
  },
  {
    "value": "6016fa7bcfcf19da94e73f15",
    "text": "Korea (South)"
  },
  {
    "value": "6016fa7b7ab386fa9ea2c5f9",
    "text": "Syria"
  },
  {
    "value": "6016fa7b7b04f61ebea526c1",
    "text": "Gabon"
  },
  {
    "value": "6016fa7bbc897535eadff58e",
    "text": "Iraq"
  },
  {
    "value": "6016fa7bfa0df2090bcb86fa",
    "text": "Yugoslavia"
  },
  {
    "value": "6016fa7b2f9c49181c4df45f",
    "text": "Tanzania"
  },
  {
    "value": "6016fa7b5f703837579e6bed",
    "text": "India"
  },
  {
    "value": "6016fa7bd6b05a58ba762b11",
    "text": "New Zealand"
  },
  {
    "value": "6016fa7bf192a64143fd4bf5",
    "text": "El Salvador"
  },
  {
    "value": "6016fa7be00e79239a0588e1",
    "text": "Mauritania"
  },
  {
    "value": "6016fa7b94bf3a69856c8c75",
    "text": "Niue"
  },
  {
    "value": "6016fa7b7180c959c6ae4bba",
    "text": "Iceland"
  },
  {
    "value": "6016fa7b5956b1df37dc4a06",
    "text": "Zimbabwe"
  },
  {
    "value": "6016fa7bf5569247b52e1b9a",
    "text": "Niger"
  },
  {
    "value": "6016fa7b0a8bc73615792a39",
    "text": "Vatican City State (Holy See)"
  },
  {
    "value": "6016fa7b79fe2eead52a31f5",
    "text": "Chile"
  },
  {
    "value": "6016fa7bd7e67c21cd73d5a8",
    "text": "Singapore"
  },
  {
    "value": "6016fa7bba72b48915a59481",
    "text": "Portugal"
  },
  {
    "value": "6016fa7b07f2f1629763c272",
    "text": "Seychelles"
  },
  {
    "value": "6016fa7b419d192902df2da6",
    "text": "Belarus"
  },
  {
    "value": "6016fa7bffea0046b1885f8e",
    "text": "Montserrat"
  },
  {
    "value": "6016fa7bfe4361ac2ac2ec40",
    "text": "Barbados"
  },
  {
    "value": "6016fa7b23788f31207e4040",
    "text": "Bolivia"
  },
  {
    "value": "6016fa7b3e5f9532bf13070e",
    "text": "St. Pierre and Miquelon"
  },
  {
    "value": "6016fa7b1cf431c59007e3b7",
    "text": "Italy"
  },
  {
    "value": "6016fa7be84e517b30359b52",
    "text": "Yemen"
  },
  {
    "value": "6016fa7b53839934e936d744",
    "text": "Togo"
  },
  {
    "value": "6016fa7b0e824ea22f267a36",
    "text": "Maldives"
  },
  {
    "value": "6016fa7bbeadd45f538cd061",
    "text": "Myanmar"
  },
  {
    "value": "6016fa7b700f980f377b44f6",
    "text": "Macedonia"
  },
  {
    "value": "6016fa7b851b478cff07eb61",
    "text": "Gibraltar"
  },
  {
    "value": "6016fa7bef2e4a91aae3d209",
    "text": "Finland"
  },
  {
    "value": "6016fa7b67b99277c29bf860",
    "text": "Lesotho"
  },
  {
    "value": "6016fa7bd59d3ea45717af09",
    "text": "Haiti"
  },
  {
    "value": "6016fa7bf18a9acf2dc9a2a1",
    "text": "Czech Republic"
  },
  {
    "value": "6016fa7ba1ff44e09988d926",
    "text": "Malawi"
  },
  {
    "value": "6016fa7bf3fb8fd10f32de6c",
    "text": "Greenland"
  },
  {
    "value": "6016fa7ba871e8dfaf9f90cc",
    "text": "Micronesia"
  },
  {
    "value": "6016fa7b926ec96a5b95eb18",
    "text": "Egypt"
  },
  {
    "value": "6016fa7bdb90686e4218afd0",
    "text": "Oman"
  },
  {
    "value": "6016fa7b10aeecd43ba6f6d8",
    "text": "Nepal"
  },
  {
    "value": "6016fa7b93bddaf2111a4c63",
    "text": "Ukraine"
  },
  {
    "value": "6016fa7bbbe72a332efac075",
    "text": "Botswana"
  },
  {
    "value": "6016fa7bfa327010c9064df3",
    "text": "French Guiana"
  },
  {
    "value": "6016fa7b46c52467748e748b",
    "text": "Panama"
  },
  {
    "value": "6016fa7be80e2b9771a18e8e",
    "text": "Viet Nam"
  },
  {
    "value": "6016fa7bedece3cf310a3f48",
    "text": "Mauritius"
  },
  {
    "value": "6016fa7be21f4274aa4b180d",
    "text": "Namibia"
  },
  {
    "value": "6016fa7bbef22177ea101d18",
    "text": "Australia"
  },
  {
    "value": "6016fa7b8041a7b31143b011",
    "text": "Malta"
  },
  {
    "value": "6016fa7b5995bd0c7b82bf59",
    "text": "Congo"
  },
  {
    "value": "6016fa7b43436a6f782b292d",
    "text": "Argentina"
  },
  {
    "value": "6016fa7be5ad79495705bf1a",
    "text": "Russian Federation"
  },
  {
    "value": "6016fa7b2fbda60ebb2eb8f6",
    "text": "Bhutan"
  },
  {
    "value": "6016fa7b7cb1acfc9314aca9",
    "text": "Cameroon"
  },
  {
    "value": "6016fa7b592fdacb6fbaa4a5",
    "text": "Philippines"
  },
  {
    "value": "6016fa7bb37ff3a353dc76de",
    "text": "Guam"
  },
  {
    "value": "6016fa7b322f9950f5b1f84c",
    "text": "Mozambique"
  },
  {
    "value": "6016fa7b1121ff206f1a62bc",
    "text": "Croatia (Hrvatska)"
  },
  {
    "value": "6016fa7b05778805802f7cc2",
    "text": "Poland"
  },
  {
    "value": "6016fa7b17240d75f87b49a9",
    "text": "Saudi Arabia"
  },
  {
    "value": "6016fa7bf8a91549e9ab5e5a",
    "text": "Antigua and Barbuda"
  },
  {
    "value": "6016fa7b5dcff73e7518596f",
    "text": "Uzbekistan"
  },
  {
    "value": "6016fa7bdd56eb7afc421ba7",
    "text": "French Polynesia"
  },
  {
    "value": "6016fa7bde4ca4fe75715eb9",
    "text": "Lebanon"
  },
  {
    "value": "6016fa7b4b90ac0d3d950b5c",
    "text": "Estonia"
  },
  {
    "value": "6016fa7b4a04ef576bb06a0e",
    "text": "Pakistan"
  },
  {
    "value": "6016fa7be8e5ed8d228c1e5f",
    "text": "Guinea-Bissau"
  },
  {
    "value": "6016fa7bf22daa4f2b7a82ca",
    "text": "Switzerland"
  },
  {
    "value": "6016fa7b79e16480105d4ace",
    "text": "Cote D'Ivoire (Ivory Coast)"
  },
  {
    "value": "6016fa7b36cb9df02103e864",
    "text": "Marshall Islands"
  },
  {
    "value": "6016fa7bad8351119ae93d33",
    "text": "Albania"
  },
  {
    "value": "6016fa7bb06da2898c092922",
    "text": "Mali"
  },
  {
    "value": "6016fa7b5b7bbce2646844bb",
    "text": "Bahamas"
  },
  {
    "value": "6016fa7ba65733b8093dc772",
    "text": "Norfolk Island"
  },
  {
    "value": "6016fa7b1f1b02d9e27c0fe4",
    "text": "France, Metropolitan"
  },
  {
    "value": "6016fa7b13a74634a78db320",
    "text": "Papua New Guinea"
  },
  {
    "value": "6016fa7bc3fe097b09da0329",
    "text": "China"
  },
  {
    "value": "6016fa7bca20340ff9880ee3",
    "text": "Greece"
  },
  {
    "value": "6016fa7b2f505f5978fbda18",
    "text": "Saint Kitts and Nevis"
  },
  {
    "value": "6016fa7b3be98e0b9dd7759d",
    "text": "Guinea"
  },
  {
    "value": "6016fa7b6fc9c11ff6f8a7be",
    "text": "Bouvet Island"
  },
  {
    "value": "6016fa7bd9366da3cf3c8f8c",
    "text": "Cayman Islands"
  },
  {
    "value": "6016fa7b240e92ebddd7e01c",
    "text": "Dominica"
  },
  {
    "value": "6016fa7be1c0cc81addd0b9b",
    "text": "Uganda"
  },
  {
    "value": "6016fa7b895dfa5677699dd2",
    "text": "Bosnia and Herzegovina"
  },
  {
    "value": "6016fa7b3aed15eced5ddf03",
    "text": "Taiwan"
  },
  {
    "value": "6016fa7b57c938517dc1ff86",
    "text": "US Minor Outlying Islands"
  },
  {
    "value": "6016fa7b0ce277ab5b84ef83",
    "text": "Turkmenistan"
  },
  {
    "value": "6016fa7b0be6f2c995a79f09",
    "text": "Faroe Islands"
  },
  {
    "value": "6016fa7bb43f8c417863dfed",
    "text": "Kyrgyzstan"
  },
  {
    "value": "6016fa7b71a6af44d0b92fa3",
    "text": "Bermuda"
  },
  {
    "value": "6016fa7b03ef99ff1af1e59a",
    "text": "France"
  },
  {
    "value": "6016fa7b6f77f8b06b71840c",
    "text": "Honduras"
  },
  {
    "value": "6016fa7b98edcec38b988f63",
    "text": "Georgia"
  },
  {
    "value": "6016fa7b67e19286a54ce78c",
    "text": "Antarctica"
  },
  {
    "value": "6016fa7b0a56afd6635893dd",
    "text": "Belize"
  },
  {
    "value": "6016fa7b6aa8d9fac227a021",
    "text": "Netherlands Antilles"
  },
  {
    "value": "6016fa7b31c5d40496a880bb",
    "text": "Korea (North)"
  },
  {
    "value": "6016fa7beeee09ddcfe4c514",
    "text": "Spain"
  },
  {
    "value": "6016fa7bffcf58ce408329b7",
    "text": "Reunion"
  },
  {
    "value": "6016fa7bd566a55ce7248adb",
    "text": "Tuvalu"
  },
  {
    "value": "6016fa7be7b8ecaff4459503",
    "text": "Latvia"
  },
  {
    "value": "6016fa7b2d8aac9858646128",
    "text": "Venezuela"
  },
  {
    "value": "6016fa7b51ee2600657a5129",
    "text": "Jordan"
  },
  {
    "value": "6016fa7bcb8f576f6b9ae813",
    "text": "Pitcairn"
  },
  {
    "value": "6016fa7b1f6caddefab562b9",
    "text": "Burundi"
  },
  {
    "value": "6016fa7b29ea7fafa3de4a91",
    "text": "Cape Verde"
  },
  {
    "value": "6016fa7b1e156abecab3101e",
    "text": "Kenya"
  },
  {
    "value": "6016fa7b5e157ef09ad4ce51",
    "text": "Tokelau"
  },
  {
    "value": "6016fa7b40c525c06a6ffee1",
    "text": "Thailand"
  },
  {
    "value": "6016fa7b49d5c4e109607265",
    "text": "Bulgaria"
  },
  {
    "value": "6016fa7ba9bcbf1eb78953b2",
    "text": "Virgin Islands (US)"
  },
  {
    "value": "6016fa7b596c1c71e3a0ff7c",
    "text": "Armenia"
  },
  {
    "value": "6016fa7b8fcd32b648c31ab8",
    "text": "Palau"
  },
  {
    "value": "6016fa7b4b83a916eec461e3",
    "text": "Svalbard and Jan Mayen Islands"
  },
  {
    "value": "6016fa7b12917fad57266770",
    "text": "Peru"
  },
  {
    "value": "6016fa7b257f9cfacb0c0c4b",
    "text": "Denmark"
  },
  {
    "value": "6016fa7b36ae0c3d03abbfa7",
    "text": "Cocos (Keeling Islands)"
  },
  {
    "value": "6016fa7bfbc67a1de23a14af",
    "text": "New Caledonia"
  },
  {
    "value": "6016fa7b6f4e0d662319498e",
    "text": "Zaire"
  },
  {
    "value": "6016fa7b0702c4eb1dad29ff",
    "text": "Laos"
  },
  {
    "value": "6016fa7b9d3834ef66b6a619",
    "text": "Saint Lucia"
  },
  {
    "value": "6016fa7ba58d9baf84504e84",
    "text": "Christmas Island"
  },
  {
    "value": "6016fa7b2781ec7a5b844550",
    "text": "Mayotte"
  },
  {
    "value": "6016fa7bd89c7caa884b70f5",
    "text": "Chad"
  },
  {
    "value": "6016fa7ba440feeb25ae6886",
    "text": "East Timor"
  },
  {
    "value": "6016fa7ba41e8b0bf497d0eb",
    "text": "Israel"
  },
  {
    "value": "6016fa7b0d44dc4140f8ec77",
    "text": "South Africa"
  },
  {
    "value": "6016fa7bbf601ba4e24f81f0",
    "text": "Heard and McDonald Islands"
  },
  {
    "value": "6016fa7b016d775945c482c5",
    "text": "Canada"
  },
  {
    "value": "6016fa7bfdd728c06c28c576",
    "text": "Nigeria"
  },
  {
    "value": "6016fa7b382b69c1bcc1fd04",
    "text": "Nauru"
  },
  {
    "value": "6016fa7bcad7dbb824c55ff9",
    "text": "Samoa"
  },
  {
    "value": "6016fa7bc05f4afb1f48702c",
    "text": "Ecuador"
  },
  {
    "value": "6016fa7b4541351a0597092d",
    "text": "Austria"
  },
  {
    "value": "6016fa7b82bfd2ec1afd6067",
    "text": "Dominican Republic"
  },
  {
    "value": "6016fa7bd64fb8b5e6ad7069",
    "text": "Belgium"
  },
  {
    "value": "6016fa7b518551f073abb436",
    "text": "Saint Vincent and The Grenadines"
  },
  {
    "value": "6016fa7b4fb6034bb1bbf0b1",
    "text": "Puerto Rico"
  },
  {
    "value": "6016fa7bccac3f6a444942e4",
    "text": "Tajikistan"
  },
  {
    "value": "6016fa7bc6f6e280ce421380",
    "text": "Luxembourg"
  },
  {
    "value": "6016fa7bdb61a544beda5b4c",
    "text": "Costa Rica"
  },
  {
    "value": "6016fa7bb3db0510d05cd4a5",
    "text": "Japan"
  },
  {
    "value": "6016fa7b83346e5dc7ec4c90",
    "text": "Brazil"
  },
  {
    "value": "6016fa7bdd661db554840ae3",
    "text": "Wallis and Futuna Islands"
  },
  {
    "value": "6016fa7b3794ac437f182f1a",
    "text": "Comoros"
  },
  {
    "value": "6016fa7b7b7aff24803eb457",
    "text": "Bangladesh"
  },
  {
    "value": "6016fa7b959e9ef5e90d7e93",
    "text": "Andorra"
  },
  {
    "value": "6016fa7b5083222fb24d3f34",
    "text": "Mongolia"
  },
  {
    "value": "6016fa7b2124e562f6ea4505",
    "text": "Zambia"
  },
  {
    "value": "6016fa7b58e68468024440d9",
    "text": "Trinidad and Tobago"
  },
  {
    "value": "6016fa7b93224a76439c946d",
    "text": "Germany"
  },
  {
    "value": "6016fa7b71def48ea41cd8aa",
    "text": "Jamaica"
  },
  {
    "value": "6016fa7bb1723a00a5b26831",
    "text": "Guatemala"
  },
  {
    "value": "6016fa7be7b77339eb5520b0",
    "text": "Moldova"
  },
  {
    "value": "6016fa7b7f009eba13a5e590",
    "text": "Sudan"
  },
  {
    "value": "6016fa7b0ecc1b4e6601fa9e",
    "text": "Angola"
  },
  {
    "value": "6016fa7b20b661506255f86a",
    "text": "Somalia"
  },
  {
    "value": "6016fa7b9e30cd7023c8e4d5",
    "text": "Afghanistan"
  },
  {
    "value": "6016fa7b5a86cbaf83aff407",
    "text": "Cyprus"
  },
  {
    "value": "6016fa7b0166590b2bc0758b",
    "text": "Ghana"
  },
  {
    "value": "6016fa7bfdd5556009ecf1b2",
    "text": "Senegal"
  },
  {
    "value": "6016fa7b47bc31c02b8114fe",
    "text": "Central African Republic"
  },
  {
    "value": "6016fa7b57abae0024c3fe88",
    "text": "Fiji"
  },
  {
    "value": "6016fa7b9173ec9ccf064300",
    "text": "Slovak Republic"
  },
  {
    "value": "6016fa7b4dbc67402658422f",
    "text": "Liberia"
  },
  {
    "value": "6016fa7bbc53929c3f43264f",
    "text": "Guyana"
  },
  {
    "value": "6016fa7b77128639a329fc87",
    "text": "Macau"
  },
  {
    "value": "6016fa7b9c826b550b9abf4b",
    "text": "Sierra Leone"
  },
  {
    "value": "6016fa7b73213c7b0419d630",
    "text": "Qatar"
  },
  {
    "value": "6016fa7b99d011e74f128bc5",
    "text": "Romania"
  },
  {
    "value": "6016fa7b6ce336ba64b602e2",
    "text": "Monaco"
  },
  {
    "value": "6016fa7b5546424c5ee28627",
    "text": "Sweden"
  },
  {
    "value": "6016fa7b72f1108e770e76f5",
    "text": "Kazakhstan"
  }
].sort((a, b) => a.text > b.text ? 1 : -1)

export const serverRequest = () => new Promise((resolve) => {
  setTimeout(() => resolve(true), 10000)
})