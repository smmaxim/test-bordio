import React, { FormEvent, useState } from 'react'
import styled from 'styled-components'
import { Button, Dropdown, DropdownItem, Checkbox, Radio, InputGroup, Form, FormGroup, Loading } from './../../../core/ui'
import { AuthBox } from './../../../features/'
import { Icons } from './../../../core/icons'
import { countries, serverRequest } from './mock'
import { schema } from './schemaValidation'

type Errors = { [key: string]: string }

export const AuthRegistrationPage: React.FC = () => {

  const [isLoad, setIsLoad] = useState<boolean>(false);
  const [errors, setErrors] = useState<Errors>({});
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [country, setCountry] = useState<null | { text: string, value: string | number }>(null)
  const [accept, setAccept] = useState<boolean>(false);
  const [gender, setGender] = useState<string>('');

  const isValid = !(name && email && password && country && accept && gender);

  const handleAcceptChange = () => setAccept(!accept);
  const handleInputChange = (e: React.FormEvent<HTMLInputElement>) => {
    interface Props { [key: string]: React.Dispatch<React.SetStateAction<string>> }
    const setters: Props = { name: setName, email: setEmail, password: setPassword, gender: setGender };
    const { name, value } = e.currentTarget;
    setters[name] && setters[name](value);
  }
  const handleFormSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!isLoad) {
      try {
        await schema.validate({ name, email, password, country: country?.value || '', accept, gender }, { abortEarly: false });
        setIsLoad(true);
        setErrors({});
        await serverRequest();
        setIsLoad(false);
      } catch (error) {
        const obj: Errors = {};
        error.inner.forEach(({ path, message }: { message: string, path: string }) => obj[path] = !obj[path] ? message : `${obj[path]}, ${message}`);
        setErrors(obj);
      }
    }
  }

  return (
    <PageLayout>
      <AuthBox title="Create a new account">
        <Form onSubmit={handleFormSubmit}>

          <FormGroup error={errors.name}>
            <InputGroup placeholder="Enter your name" value={name} name="name" onChange={handleInputChange} />
          </FormGroup>

          <FormGroup error={errors.email}>
            <InputGroup placeholder="Email" iconLeft={<Icons.mail />} value={email} name="email" onChange={handleInputChange} />
          </FormGroup>

          <FormGroup error={errors.password}>
            <InputGroup type="password" placeholder="Password" value={password} name="password" onChange={handleInputChange} iconLeft={<Icons.lock />} />
          </FormGroup>

          <FormGroup error={errors.country}>
            <Dropdown label="Select country" value={country} onChange={setCountry}>
              {countries.map(({ text, value }) => (
                <DropdownItem key={value} value={value}>{text}</DropdownItem>
              ))}
            </Dropdown>
          </FormGroup>

          <FormGroup error={errors.accept}>
            <Checkbox checked={accept} onChange={handleAcceptChange}>Accept <a href="#terms" target="_blank">terms</a> and <a href="#conditions" target="_blank">conditions</a></Checkbox>
          </FormGroup>

          <FormGroup error={errors.gender}>
            <Radio name="gender" value="male" currentValue={gender} onChange={handleInputChange}>Male</Radio>
            <Radio name="gender" value="female" currentValue={gender} onChange={handleInputChange}>Female</Radio>
          </FormGroup>

          <FormGroup>
            <StyledButton disabled={isValid} variant="primary" type="submit" fullWidth>
              {isLoad ? <Loading size={30} /> : 'Sing up'}
            </StyledButton>
          </FormGroup>

        </Form>
      </AuthBox>
    </PageLayout>
  )

}

const PageLayout = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`

const StyledButton = styled(Button)`
  position: relative;
  & ${Loading} {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -15px;
    margin-left: -15px;
  }
`